import React, { Component } from "react";
import Header from "./Components/Header/Header";
import Home from "./Components/Home/Home";
import AddContact from "./Components/AddPage/AddPage";
import { BrowserRouter as Router, Route } from "react-router-dom";

import "./App.css";

class App extends Component {
  constructor() {
    super();
    this.state = {
      contacts: [
        {
          name: "santosh",
          phone: "543454323"
        },
        {
          name: "ramesh",
          phone: "876432324"
        }
      ]
    };
    console.log(this.state.contacts);
  }

  addContact = data => {
    console.log("addCOntact", data);
    this.setState({ contacts: data });
  };

  deleteContact = index => {
    console.log("Here");
    let copy = Object.assign([], this.state.contacts);
    copy.splice(index, 1);
    this.setState({ contacts: copy });
  };

  render() {
    return (
      <div className="App">
        <Header heading="PHONE DIRECTORY" />
        <Router>
          <Route
            exact
            path="/"
            render={() => (
              <Home
                contacts={this.state.contacts}
                deleteContact={this.deleteContact.bind(this)}
              />
            )}
          />
          <Route
            path="/add"
            render={() => (
              <AddContact
                contacts={this.state.contacts}
                addContact={this.addContact.bind(this)}
              />
            )}
          />
        </Router>
      </div>
    );
  }
}

export default App;
