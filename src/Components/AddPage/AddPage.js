import React, { Component } from "react";
import "./AddPage.css";
import { Link } from "react-router-dom";

class AddContact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      phone: ""
    };
    console.log(this.props.contacts);
    this.addContact = this.addContact.bind(this);
  }

  onChange(event) {
    console.log(event.target.value);
    this.setState({ name: event.target.value });
  }

  onChangePhone(event) {
    console.log(event.target.value);
    this.setState({ phone: event.target.value });
  }

  addContact() {
    let copy = [];
    copy = this.props.contacts;
    console.log(copy);
    copy.push(this.state);
    console.log(copy);
    //now I'll call that function from the props.
    //this.props.addContact(this.state);
  }

  render() {
    return (
      <div>
        <Link to="/">
          <div className="back-btn">BACK</div>
        </Link>

        <div className="input-section">
          <label>Name:</label>
          <br />
          <input
            type="text"
            id="Name"
            className="form-control"
            onChange={this.onChange.bind(this)}
          />
        </div>

        <div className="input-section">
          <label>Phone:</label>
          <br />
          <input
            type="text"
            id="Phone"
            className="form-control"
            onChange={this.onChangePhone.bind(this)}
          />
        </div>

        <div className="user">
          <b>Suscriber to be added:</b>
          <br />
          <label>Name: {this.state.name}</label>
          <br />
          <label>Phone: {this.state.phone}</label>
          <br />
        </div>

        <Link to="/">
          <div className="add-btn" onClick={this.addContact}>
            ADD
          </div>
        </Link>
      </div>
    );
  }
}

export default AddContact;
